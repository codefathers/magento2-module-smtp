<?php

namespace Cf\Smtp\Api;



/**
 * Interface TransportConfigInterface
 */
interface TransportConfigInterface
{

    /**
     * @return bool
     */
    public function isEnabled();


    /**
     * @return bool
     */
    public function isTypeSmtp();

    /**
     * @return bool
     */
    public function isTypeDefault();


    /**
     * @return bool
     */
    public function useCustomReturnPath();

    /**
     * @return bool
     */
    public function useDefaultReturnPath();


    /**
     * @param Store|null $store
     */
    public function getReturnPathEmail();


    /**
     * @return array
     */
    public function getTransportOptions();

}
