<?php

namespace Cf\Smtp\Plugin\Mail\Template;

use Magento\Framework\Registry;

/**
 * Class TransportBuilder
 */
class TransportBuilder
{
    /**
     * @var Registry $registry
     */

    protected $registry;

    /**
     * @param Registry $registry
     */
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @param \Magento\Framework\Mail\Template\TransportBuilder $subject
     * @param array $templateOptions
     */
    public function beforeSetTemplateOptions(
        \Magento\Framework\Mail\Template\TransportBuilder $subject,
        $templateOptions
    )
    {
        $this->registry->unregister('smtp_template_options');
        $this->registry->register('smtp_template_options', $templateOptions);
    }
}
