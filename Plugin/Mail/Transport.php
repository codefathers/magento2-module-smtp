<?php


namespace Cf\Smtp\Plugin\Mail;

use Magento\Framework\Exception\MailException;
use Magento\Framework\Mail\TransportInterface;
use Magento\Framework\Phrase;

/**
 * Class Transport
 */
class Transport
{

    /**
     * @var \Magento\Framework\Registry $registry
     */
    protected $registry;

    /**
     * @var \Cf\Smtp\Model\ConfigFactory
     */
    protected $configFactory;

    /**
     * @var \Cf\Smtp\Model\Mail\TransportFactory
     */
    protected $transportFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;


    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $storeManager;


    /**
     * Transport constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Registry $registry
     * @param \Cf\Smtp\Model\ConfigFactory $config
     * @param \Cf\Smtp\Model\Mail\TransportFactory $helper
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        \Cf\Smtp\Model\ConfigFactory $configFactory,
        \Cf\Smtp\Model\Mail\TransportFactory $transportFactory
    )
    {
        $this->storeManager = $storeManager;
        $this->registry = $registry;
        $this->configFactory = $configFactory;
        $this->transportFactory = $transportFactory;
    }

    /**
     * @param TransportInterface $subject
     * @param \Closure $proceed
     * @throws \Magento\Framework\Exception\MailException | \Exception
     *
     * @return null
     */
    public function aroundSendMessage(
        TransportInterface $subject,
        \Closure $proceed
    )
    {
        $options = $this->getOptions();
        if (!$options) {
            return $proceed();
        }

        $storeId = (isset($options['store'])) ? (int) $options['store'] : null;
        if (!$storeId) {
            return $proceed();
        }
        try {
            $store = $this->storeManager->getStore($storeId);
        } catch (\Exception $e) {
            throw new MailException(new Phrase($e->getMessage()), $e);
        }

        /** @var \Cf\Smtp\Api\TransportConfigInterface $config */
        $config = (isset($options['transport_options'])) ? $options['transport_options'] : $this->configFactory->get($store);

        if (!$config || !$config->isEnabled() || !$config->isTypeSmtp()) {
            return $proceed();
        }

        /** @var \Magento\Framework\Mail\MessageInterface $message */
        $message = $subject->getMessage();
        if (!$message) {
            return $proceed();
        }

        /** @var string $returnPath */
        $useCustomReturnPath = $config->useCustomReturnPath();
        $returnPath = $config->getReturnPathEmail();
        if ($useCustomReturnPath && $returnPath) {
            $message->setReturnPath($returnPath);
        }

        /** @var TransportInterface $transport */
        try {
            $transport = $this->transportFactory->create($config);
            $transport->send($message);
        } catch (\Exception $e) {
            throw new MailException(new Phrase($e->getMessage()), $e);
        }
    }

    /**
     * @return mixed
     */
    protected function getOptions()
    {
        $result = $this->registry->registry('smtp_template_options');
        $this->registry->unregister('smtp_template_options');
        return $result;
    }



}
