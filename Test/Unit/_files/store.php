<?php
/**
 * GFE Themeworld - Magento 2 Module
 * Copyright (c) 2018 GFE | Media GmbH
 * @website    https://www.gfe-media.de/
 * @contact    info@gfe-media.de
 * @author     Achim Schweisgut <achim@schweisgut.expert>
 * @package    Gfe_Themeworld
 */

$storeManager = Magento\TestFramework\Helper\Bootstrap::getObjectManager()->get(\Magento\Store\Model\StoreManagerInterface::class);
$store = \Magento\TestFramework\Helper\Bootstrap::getObjectManager()->create(\Magento\Store\Model\Store::class);

$websiteId = $storeManager->getWebsite()->getId();
$groupId = $storeManager->getWebsite()->getDefaultGroupId();

$store
    ->setId(null)
    ->setCode('fixture_second_store')
    ->setWebsiteId($websiteId)
    ->setGroupId($groupId)
    ->setName('Fixture Store 2')
    ->setSortOrder(10)
    ->setIsActive(1)
    ->save();

$store
    ->setId(null)
    ->setCode('fixture_third_store')
    ->setWebsiteId($websiteId)
    ->setGroupId($groupId)
    ->setName('Fixture Store 3')
    ->setSortOrder(20)
    ->setIsActive(1)
    ->save();


/* Refresh stores memory cache */
$storeManager->reinitStores();
