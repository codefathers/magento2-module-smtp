<?php


namespace Cf\Smtp\Test\Unit\Model;


use Magento\TestFramework\Helper\Bootstrap;

/**
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @codingStandardsIgnoreFile
 *
 * @group Cf_Smtp
 */
class MailTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $om;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;


    protected function setUp()
    {
        $this->om = Bootstrap::getObjectManager();
        $this->transportBuilder = $this->om->get('Magento\Framework\Mail\Template\TransportBuilder');
    }

    /**
     * @test
     * @group current
     */
    public function sendTestMail()
    {
        $data = [
            'adapter'    => 'smtp',
            'host'       => 'psa22.webhoster.ag',
            'port'       => '25',
            'auth'       => 'plain',
            'username'   => 'shop@sotaparts.com',
            'password'   => 'Eqqx30~7',
        ];

        $config = new \Cf\Smtp\Model\CustomConfig($data);

        $this->transportBuilder
            ->setTemplateIdentifier('cf_smtp_test_email_template')
            ->setTemplateOptions([
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                'config' => $config
            ])
            ->setTemplateVars([])
            ->setFrom(['name' => 'otto', 'email' =>"info@sotaparts.com"])
            ->addTo("a.schweisgut@gmail.com");

        $this->transportBuilder
            ->getTransport()
            ->sendMessage();
    }



}
