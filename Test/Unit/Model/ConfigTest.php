<?php


namespace Cf\Smtp\Test\Unit\Model;


use Magento\TestFramework\Helper\Bootstrap;

/**
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @codingStandardsIgnoreFile
 *
 * @group Cf_Smtp
 */
class ConfigTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $om;

    /**
     * @var \Cf\Smtp\Model\ConfigFactory
     */
    private $factory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;


    protected function setUp()
    {
        $this->om = Bootstrap::getObjectManager();
        $this->factory = $this->om->get('\Cf\Smtp\Model\ConfigFactory');
        $this->storeManager = $this->om->get('\Magento\Store\Model\StoreManagerInterface');

    }


    /**
     *
     */
    public static function loadStoreFixture()
    {
        include __DIR__ . '/../_files/store.php';
    }


    /**
     *
     * @test
     * @magentoConfigFixture current_store system/smtp/disable 0
     * @magentoConfigFixture current_store system/smtp/adapter smtp
     * @magentoConfigFixture current_store system/smtp/protocol ssl
     * @magentoConfigFixture current_store system/smtp/auth_type plain
     *
     * @magentoConfigFixture current_store system/smtp/host smtp.test.com
     * @magentoConfigFixture current_store system/smtp/port 26
     * @magentoConfigFixture current_store system/smtp/login unittest
     * @magentoConfigFixture current_store system/smtp/password pw123
     *
     * @magentoConfigFixture current_store system/smtp/set_return_path 2
     * @magentoConfigFixture current_store system/smtp/return_path_email test@codefathers.de
     *
     */
    public function checkConfig()
    {
        $store = $this->storeManager->getStore();
        /** @var \Cf\Smtp\Model\Config $config */
        $config = $this->factory->create($store);

        $this->assertTrue($config->isEnabled());
        $this->assertEquals('smtp', $config->getAdapter());
        $this->assertTrue($config->isTypeSmtp());
        $this->assertEquals('ssl', $config->getProtocol());
        $this->assertEquals('plain', $config->getAuthType());

        $this->assertEquals('smtp.test.com', $config->getHost());
        $this->assertEquals(26, $config->getPort());
        $this->assertEquals('unittest', $config->getLogin());
        $this->assertEquals('pw123', $config->getPassword());

        $this->assertTrue($config->useCustomReturnPath());
        $this->assertEquals('test@codefathers.de', $config->getReturnPathEmail());


        $options =  $config->getTransportOptions();

        $this->assertInternalType('array', $options);
        $this->assertEquals('smtp.test.com', $options['host']);
        $this->assertEquals(26, $options['port']);
    }



}
