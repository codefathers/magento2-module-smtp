<?php


namespace Cf\Smtp\Test\Unit\Model;


use Magento\TestFramework\Helper\Bootstrap;

/**
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @codingStandardsIgnoreFile
 *
 * @group Cf_Smtp
 */
class ConfigFactoryTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $om;

    /**
     * @var \Cf\Smtp\Model\ConfigFactory
     */
    private $factory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    protected function setUp()
    {
        $this->om = Bootstrap::getObjectManager();
        $this->factory = $this->om->get('\Cf\Smtp\Model\ConfigFactory');
        $this->storeManager = $this->om->get('\Magento\Store\Model\StoreManagerInterface');
    }


    /**
     *
     */
    public static function loadStoreFixture()
    {
        include __DIR__ . '/../_files/store.php';
    }

    /**
     *
     * @test
     *
     */
    public function checkModel()
    {
        $this->assertInstanceOf('\Cf\Smtp\Model\ConfigFactory', $this->factory);
    }

    /**
     *
     * @test
     *
     */
    public function createMustReturnConfigModel()
    {
        $config = $this->factory->create();
        $this->assertInstanceOf('\Cf\Smtp\Model\Config', $config);
    }

    /**
     *
     * @test
     * @magentoDataFixture loadStoreFixture
     *
     */
    public function createMustReturnNewConfig()
    {
        $store1 = $this->storeManager->getStore('fixture_second_store');
        $store2 = $this->storeManager->getStore('fixture_third_store');
        $config1 = $this->factory->create($store1);
        $config2 = $this->factory->create($store1);
        $config3 = $this->factory->create($store2);
        $this->assertNotSame($config1, $config2);
        $this->assertNotSame($config1, $config3);
        $this->assertNotSame($config2, $config3);
    }

    /**
     *
     * @test
     * @magentoDataFixture loadStoreFixture
     *
     */
    public function getMustReturnInstancesByStore()
    {
        $store1 = $this->storeManager->getStore('fixture_second_store');
        $store2 = $this->storeManager->getStore('fixture_third_store');
        $config1 = $this->factory->get($store1);
        $config2 = $this->factory->get($store1);
        $config3 = $this->factory->get($store2);
        $this->assertSame($config1, $config2);
        $this->assertNotSame($config2, $config3);
    }

}
