<?php


namespace Cf\Smtp\Test\Unit\Helper;


use Magento\TestFramework\Helper\Bootstrap;

/**
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @codingStandardsIgnoreFile
 *
 * @group Cf_Smtp
 */
class DataTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $om;

    protected function setUp()
    {
        $this->om = Bootstrap::getObjectManager();
    }

    /**
     *
     * @test
     *
     */
    public function checkModel()
    {
        $helper = $this->om->get("Cf\Smtp\Helper\Data");
        $this->assertInstanceOf("Cf\Smtp\Helper\Data", $helper);
    }



}
