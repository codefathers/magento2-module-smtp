<?php

namespace Cf\Smtp\Model;

use Magento\Store\Api\Data\StoreInterface as Store;
use Magento\Store\Model\ScopeInterface;


/**
 * Class Config
 */
class Config implements \Cf\Smtp\Api\TransportConfigInterface
{

    /** @var string */
    const PATH = 'system/smtp';


    /** @var \Magento\Store\Api\Data\StoreInterface */
    protected $store;

    /** @var \Magento\Framework\App\Config\ScopeConfigInterface */
    protected $scopeConfig;

    /**
     * Config constructor.
     * @param Context $context
     * @param StoreManager $storeManager
     */
    public function __construct(
        \Magento\Store\Api\Data\StoreInterface $store,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->store = $store;
        $this->scopeConfig = $scopeConfig;
    }


    /**
     * @return string
     */
    protected function getValue($var)
    {
        $path = self::PATH . '/' . $var;
        $result = $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, $this->store);
        return $result;
    }

    /**
     * @return string
     */
    protected function getString($key)
    {
        return (string)$this->getValue($key);
    }

    /**
     * @return int
     */
    protected function getInt($key)
    {
        return (int)$this->getValue($key);
    }

    /**
     * @return bool
     */
    protected function getBool($key)
    {
        $state = trim(strtolower((string)$this->getValue($key)));
        if (in_array($state, ['1', 'yes', 'enabled', 'true', 'ok', 'yep'])) {
            return true;
        }
        if (in_array($state, ['0', 'no', 'disabled', 'false', 'none'])) {
            return false;
        }
        return (bool)$state;
    }


    /**
     * @return bool
     */
    public function isEnabled()
    {
        $disabled = $this->getBool('disable');
        return ($disabled) ? false : true;
    }

    /**
     * @return string
     */
    public function getAdapter()
    {
        $result = $this->getString('adapter');
        return $result;
    }


    /**
     * @return bool
     */
    public function isTypeSmtp()
    {
        $adapter = $this->getAdapter();
        return ($adapter == 'smtp') ? true : false;
    }

    /**
     * @return bool
     */
    public function isTypeDefault()
    {
        $adapter = $this->getAdapter();
        return ($adapter == 'default') ? true : false;
    }


    /**
     * @return string
     */
    public function getHost()
    {
        $result = $this->getString('host');
        return $result;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        $result = $this->getInt('port');
        return $result;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        $result = $this->getString('login');
        return $result;
    }


    /**
     * @return string
     */
    public function getPassword()
    {
        $result = $this->getString('password');
        return $result;
    }


    /**
     * @return bool
     */
    public function useCustomReturnPath()
    {
        $state = $this->getInt('set_return_path');
        $email = $this->getReturnPathEmail();
        return ($state == 2 && $email) ? true : false;
    }

    /**
     * @return bool
     */
    public function useDefaultReturnPath()
    {
        $state = $this->getInt('set_return_path');
        return ($state == 1) ? true : false;
    }

    /**
     * @param Store|null $store
     */
    public function getReturnPathEmail()
    {
        $result = $this->getString('return_path_email');
        return $result;
    }

    /**
     * @param Store|null $store
     * @return string|void
     */
    public function getAuthType()
    {
        $result = $this->getString('auth_type');
        return ($result !== 'none') ? $result : '';
    }


    /**
     * @param Store|null $store
     * @return string|void
     */
    public function getProtocol()
    {
        $result = $this->getString('protocol');
        return ($result !== 'none') ? $result : '';
    }


    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getTransportOptions()
    {
        $authType = $this->getAuthType();
        $protocol = $this->getProtocol();

        $result = array(
            'host' => $this->getHost(),
            'port' => $this->getPort(),
            'auth' => $authType
        );
        if ($authType !== 'none') {
            $result['username'] = $this->getLogin();
            $result['password'] = $this->getPassword();
        }

        if ($protocol) {
            $result['ssl'] = $protocol;
        }
        return $result;
    }

}
