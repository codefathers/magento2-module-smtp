<?php

namespace Cf\Smtp\Model\Mail;

use Zend_Application_Resource_Mail as MailResource;
use Cf\Smtp\Api\TransportConfigInterface;


/**
 * Class TransportFactory
 */
class TransportFactory
{


    /**
     * @param \Cf\Smtp\Api\TransportConfigInterface $config
     * @return mixed|null|\Zend_Mail_Transport_Abstract
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function create(TransportConfigInterface $config)
    {
        $options = $config->getTransportOptions();
        $options['type'] = 'smtp';
        $res = new MailResource(['transport' => $options]);
        $result = $res->init();
        return $result;
    }


}
