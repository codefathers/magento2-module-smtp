<?php

namespace Cf\Smtp\Model;

use Magento\Store\Api\Data\StoreInterface as Store;
use Magento\Store\Model\ScopeInterface;


/**
 * Class Config
 */
class CustomConfig
    extends \Magento\Framework\DataObject
    implements \Cf\Smtp\Api\TransportConfigInterface
{


    /**
     * @return bool
     */
    public function isEnabled()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getAdapter()
    {
        return "smtp";
    }


    /**
     * @return bool
     */
    public function isTypeSmtp()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isTypeDefault()
    {
        return false;
    }



    /**
     * @return bool
     */
    public function useCustomReturnPath()
    {
        return ($this->hasReturnPathEmail()) ? true : false;
    }

    /**
     * @return bool
     */
    public function useDefaultReturnPath()
    {
        return false;
    }

    /**
     * @return mixed|null
     */
    public function getReturnPathEmail()
    {
        $result = $this->getData('return_path_email');
        return $result ? $result : null;
    }


    /**
     * @return array
     */
    public function getTransportOptions()
    {
        $result = $this->getData();
        if ($result['auth'] === 'none') {
            unset($result['auth']);
            unset($result['username']);
            unset($result['password']);
        }
        return $result;
    }

}
